import test from 'ava';
import m from './index';

const format = (s: string) => JSON.stringify(JSON.parse(s));

test('replace comments with whitespace', t => {
	const c1 = `//comment\n{"a":"b"}`;
	const c2 = `/*//comment*/{"a":"b"}`;
	const c3 = `{"a":"b"//comment\n}`;
	const c4 = `{"a":"b"/*comment*/}`;
	const c5 = `{"a"/*\r\n\r\n\r\ncomment\r\n*/:"b"}`;
	const c6 = `/*!\n * comment\n */\n{"a":"b"}`;
	const c7 = '{/*comment*/"a":"b"}';
	const expected = format('{"a":"b"}');

	t.is(format(m(c1)), expected);
	t.is(format(m(c2)), expected);
	t.is(format(m(c3)), expected);
	t.is(format(m(c4)), expected);
	t.is(format(m(c5)), expected);
	t.is(format(m(c6)), expected);
	t.is(format(m(c7)), expected);
});

test('remove comments', t => {
	t.is(m('//comment\n{"a":"b"}'), '\n{"a":"b"}');
	t.is(m('/*//comment*/{"a":"b"}'), '{"a":"b"}');
	t.is(m('{"a":"b"//comment\n}'), '{"a":"b"\n}');
	t.is(m('{"a":"b"/*comment*/}'), '{"a":"b"}');
	t.is(m('{"a"/*\n\n\ncomment\r\n*/:"b"}'), '{"a":"b"}');
	t.is(m('/*!\n * comment\n */\n{"a":"b"}'), '\n{"a":"b"}');
	t.is(m('{/*comment*/"a":"b"}'), '{"a":"b"}');
});

test("doesn't strip comments inside strings", t => {
	t.is(m('{"a":"b//c"}'), '{"a":"b//c"}');
	t.is(m('{"a":"b/*c*/"}'), '{"a":"b/*c*/"}');
	t.is(m('{"/*a":"b"}'), '{"/*a":"b"}');
	t.is(m('{"\\"/*a":"b"}'), '{"\\"/*a":"b"}');
});

test('consider escaped slashes when checking for escaped string quote', t => {
	t.is(m('{"\\\\":"https://foobar.com"}'), '{"\\\\":"https://foobar.com"}');
	t.is(m('{"foo\\"":"https://foobar.com"}'), '{"foo\\"":"https://foobar.com"}');
});

test('line endings - no comments', t => {
	t.is(m('{"a":"b"\n}'), '{"a":"b"\n}');
	t.is(m('{"a":"b"\r\n}'), '{"a":"b"\r\n}');
});

test('line endings - single line comment', t => {
	const c1 = '{"a":"b"//c\n}';
	const c2 = '{"a":"b"//c\r\n}';
	const expected = format('{"a":"b"   \n}');

	t.is(format(m(c1)), expected);
	t.is(format(m(c2)), expected);
});

test('line endings - single line block comment', t => {
	const c1 = '{"a":"b"/*c*/\n}';
	const c2 = '{"a":"b"/*c*/\r\n}';
	const expected1 = format('{"a":"b"    \n}');
	const expected2 = format('{"a":"b"     \r\n}');

	t.is(format(m(c1)), expected1);
	t.is(format(m(c2)), expected2);
});

test('line endings - multi line block comment', t => {
	const c1 = '\n{\n"a":"b",/*c\nc2*/"x":"y"\n}';
	const c2 = '{"a":"b",/*c\r\nc2*/"x":"y"\r\n}';
	const expected1 = format('{"a":"b",   \n    "x":"y"\n}');
	const expected2 = format('{"a":"b",   \r\n    "x":"y"\r\n}');

	t.is(format(m(c1)), expected1);
	t.is(format(m(c2)), expected2);
});

// test.failing('handles weird escaping', t => {
// eslint-disable-next-line no-useless-escape
// t.is(
// m('{"x":"x "sed -e \\"s/^.\\\\{46\\\\}T//\\" -e \\"s/#033/\\\\x1b/g\\"""}'),
// '{"x":"x "sed -e \\"s/^.\\\\{46\\\\}T//\\" -e \\"s/#033/\\\\x1b/g\\"""}'
// );
// });
