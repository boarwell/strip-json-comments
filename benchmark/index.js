/* globals bench, set */
'use strict';
const fs = require('fs');
const stripJsonComments = require('../build/index').default;

const json = fs.readFileSync('benchmark/sample.json', 'utf8');
const bigJson = fs.readFileSync('benchmark/sample-big.json', 'utf8');
console.log(stripJsonComments);

bench('strip JSON comments', () => {
	set('type', 'static');
	stripJsonComments(json);
});

bench('strip JSON comments without whitespace', () => {
	stripJsonComments(json);
});

bench('strip Big JSON comments', () => {
	stripJsonComments(bigJson);
});
